module.exports = {
  "extends": ["prettier", "prettier/react"],
  "parser": "babel-eslint",
  "parserOptions": {
    "sourceType": "module",
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true,
      "jsx": true
    }
  },
  "settings": {
    "import/core-modules": []
  },
  "globals": {
    "graphql": true,
    "tw": true
  },
  "env": {
    "browser": true,
    "node": true
  },
  "plugins": ["prettier"],
  "rules": {
    "prettier/prettier": [
      "error"
    ],
    "react/prop-types": "off",
    "import/prefer-default-export": "off"
  }
};
  