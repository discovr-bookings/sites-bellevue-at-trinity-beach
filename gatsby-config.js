module.exports = {
  /* Plugins */
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-layout',
    '@bit/discovrbookings.library.sites.plugin-segment-js',
  ],
};
