/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
const React = require('react');
const fs = require('fs');

function getFilesFromPath(path, extension) {
	const dir = fs.readdirSync(path);
	return dir.filter(elm => elm.match(new RegExp(`.*.(${extension})`, 'ig')));
}

exports.onRenderBody = ({ setHeadComponents }) => {
	const eot = getFilesFromPath('./public/static', '.eot');
	const woff = getFilesFromPath('./public/static', '.woff');
	const woff2 = getFilesFromPath('./public/static', '.woff2');

	const preload = [
		'gtAmerica-light',
		'gtAmerica-medium',
		'gtAmerica-regular',
		'gtSuperDisplay-light'
	];

	setHeadComponents([
		eot.map(file =>
			preload.map((font, key) => {
				const fileBeginning = file
					.split('-')
					.slice(0, -1)
					.join('-');
				if (fileBeginning === font) {
					return (
						<link
							key={key}
							rel="preload"
							as="font"
							type="font/eot"
							crossOrigin="anonymous"
							href={`/static/${file}`}
						/>
					);
				}
			})
		),
		woff.map(file =>
			preload.map((font, key) => {
				const fileBeginning = file
					.split('-')
					.slice(0, -1)
					.join('-');
				if (fileBeginning === font) {
					return (
						<link
							key={key}
							rel="preload"
							as="font"
							type="font/woff"
							crossOrigin="anonymous"
							href={`/static/${file}`}
						/>
					);
				}
			})
		),
		woff2.map(file =>
			preload.map((font, key) => {
				const fileBeginning = file
					.split('-')
					.slice(0, -1)
					.join('-');
				if (fileBeginning === font) {
					return (
						<link
							key={key}
							rel="preload"
							as="font"
							type="font/woff2"
							crossOrigin="anonymous"
							href={`/static/${file}`}
						/>
					);
				}
			})
		)
	]);
};
