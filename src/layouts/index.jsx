import React, { Component, useContext } from 'react';
import { ThemeProvider } from 'styled-components';
import getFirebase, {
  FirebaseContext,
} from '@bit/discovrbookings.library.sites.provider-firebase';
import {
  AdminProvider,
  AdminConsumer,
} from '@bit/discovrbookings.library.sites.provider-admin';
import { withAuthentication } from '@bit/discovrbookings.library.sites.provider-auth';
import { ConsentManager } from '@bit/discovrbookings.library.sites.consent-manager';
import LayoutManager from '@bit/discovrbookings.library.sites.layout-manager';

import { GlobalProvider } from '../components/contextProvider';
import { Theme } from '../components/global/theme';

const consentOption = {
  breakpoint: props => props.theme.bp.s,
  bodyFont: props => props.theme.f.body,
  displayFont: props => props.theme.f.display,
  backgroundColor: props => props.theme.c.white,
  buttonColor: props => props.theme.c.white,
  textColor: props => props.theme.c.colorA,
  primaryColor: props => props.theme.c.colorA,
  secondaryColor: props => props.theme.c.colorB,
  writeKey: process.env.GATSBY_SEGMENT_WRITE_KEY,
};

const PageLayout = withAuthentication(({ children }) => {
  const { admin } = useContext(AdminConsumer);
  console.log(admin);
  return (
    <LayoutManager adminProps={admin}>
      <div style={{ background: 'black', minHeight: '100vh' }}>{children}</div>
    </LayoutManager>
  );
});

class Layout extends Component {
  state = {
    firebase: null,
  };

  componentDidMount() {
    const app = import('firebase/app');
    const auth = import('firebase/auth');
    const firestore = import('firebase/firestore');
    const performance = import('firebase/performance');
    const analytics = import('firebase/analytics');
    Promise.all([app, auth, firestore, performance, analytics]).then(values => {
      const firebase = getFirebase(values[0]);
      this.setState({ firebase });
    });
  }

  render() {
    const { firebase } = this.state;
    const { location, children } = this.props;
    return (
      <FirebaseContext.Provider value={firebase}>
        <ThemeProvider theme={Theme}>
          <GlobalProvider location={location}>
            <AdminProvider location={location}>
              <ConsentManager {...consentOption} />
              <PageLayout>{children}</PageLayout>
            </AdminProvider>
          </GlobalProvider>
        </ThemeProvider>
      </FirebaseContext.Provider>
    );
  }
}

export default Layout;
