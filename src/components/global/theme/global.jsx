import { createGlobalStyle, css } from 'styled-components';

// Styles
import Normalize from './normalize';
import Fonts from './fonts';

const GlobalCSS = css`
  html {
    box-sizing: border-box;
    font-size: 62.5%;
  }

  body,
  html {
    padding: 0;
    margin: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  body {
    height: 100%;
    position: absolute;
    background-color: ${props => props.theme.c.white};
    color: ${props => props.theme.c.colorA};
    font-family: ${props => props.theme.f.body};
    font-style: normal;
    font-weight: 400;
  }

  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    @media print {
      background: transparent !important;
      color: #000 !important;
      box-shadow: none !important;
      text-shadow: none !important;
      filter: none !important;
    }
  }

  a {
    @media print {
      text-decoration: underline;
      &:visited {
        text-decoration: underline;
      }
      &[href]:after {
        content: ' (' attr(href) ')';
      }
      &[href^='#']:after {
        content: '';
      }
    }
  }

  p {
    line-height: 1.55;
  }

  b,
  strong {
    font-weight: 700;
  }

  h1,
  h2,
  h3,
  h4,
  h5 {
    font-family: ${props => props.theme.f.display};
    font-style: normal;
    font-weight: 400;
    line-height: 1;
  }

  h1 + h1,
  h1 + h2,
  h2 + h1,
  h2 + h2 {
    margin-top: 2.1rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 1.8rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 1.7rem;
    }
  }

  h4 + h1,
  h4 + h2,
  h5 + h1,
  h5 + h2,
  h6 + h1,
  h6 + h2 {
    margin-top: 1.5rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 1.2rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 0.7rem;
    }
  }

  ol + h1,
  ol + h2,
  ul + h1,
  ul + h2 {
    margin-top: 4rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 4.6rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 4.3rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 4rem;
    }
  }

  p + h1,
  p + h2 {
    margin-top: 7.2rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 8.8rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 11.2rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 12.8rem;
    }
  }

  h2 + h3,
  h3 + h3 {
    margin-top: 2.1rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 1.8rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 1.7rem;
    }
  }

  ol + h3,
  p + h3,
  ul + h3 {
    margin-top: 4rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 4.6rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 4.3rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 4rem;
    }
  }

  h2 + h4,
  h2 + h5,
  h2 + h6,
  h3 + h4,
  h3 + h5,
  h3 + h6,
  h4 + h4,
  h4 + h5,
  h4 + h6,
  h5 + h4,
  h5 + h5,
  h5 + h6,
  h6 + h4,
  h6 + h5,
  h6 + h6 {
    margin-top: 2.1rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 1.8rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 1.7rem;
    }
  }

  ol + h4,
  ol + h5,
  ol + h6,
  ul + h4,
  ul + h5,
  ul + h6 {
    margin-top: 4rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 4.6rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 4.3rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 4rem;
    }
  }

  p + h4,
  p + h5 {
    margin-top: 5rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 5.6rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 5.3rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 3.4rem;
    }
  }

  h1 + ol,
  h1 + ul,
  h2 + ol,
  h2 + ul,
  h3 + ol,
  h3 + ul,
  h4 + ol,
  h4 + ul,
  h5 + ol,
  h5 + ul,
  h6 + ol,
  h6 + ul {
    margin-top: 2.5rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 2rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 1.8rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 2.5rem;
    }
  }

  p + ol,
  p + ul {
    margin-top: 4rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 4.6rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 4.3rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 4rem;
    }
  }

  ol + p,
  ul + p {
    margin-top: 4rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 4.6rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 4.3rem;
    }
  }

  ul > li > ul {
    margin-top: 2rem;
  }

  h1 + p,
  h2 + p,
  h3 + p {
    margin-top: 1.6rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 1.2rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 1rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 0.9rem;
    }
  }

  h4 + p,
  h5 + p {
    margin-top: 0.8rem;
  }

  h6 + p,
  p + h6 {
    margin-top: 3rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      margin-top: 3.6rem;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      margin-top: 3.3rem;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      margin-top: 3rem;
    }
  }

  h1 {
    line-height: 4.8rem;
    font-size: 4.4rem;
    @media (min-width: ${props => props.theme.bp.s}) {
      font-size: 5rem;
      line-height: 1.12;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      font-size: 6.4rem;
      line-height: 1.125;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      font-size: 5.6rem;
      line-height: 1;
    }
  }

  h2 {
    font-size: 3.4rem;
    letter-spacing: -0.0052em;
    line-height: 1.14;
    @media (min-width: ${props => props.theme.bp.s}) {
      font-size: 3rem;
      letter-spacing: -0.007em;
      line-height: 1.26;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      font-size: 3.8rem;
      letter-spacing: -0.0083em;
      line-height: 1.24;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      font-size: 4.8rem;
      letter-spacing: -0.001em;
      line-height: 1.18;
    }
  }

  h3 {
    font-size: 3rem;
    letter-spacing: -0.0043em;
    line-height: 1.39;
    @media (min-width: ${props => props.theme.bp.s}) {
      font-size: 2.8rem;
      letter-spacing: -0.0052em;
      line-height: 1.28;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      font-size: 3.2rem;
      letter-spacing: -0.0059em;
      line-height: 1.25;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      font-size: 3.6rem;
      letter-spacing: -0.0067em;
      line-height: 1.33;
    }
  }

  h4,
  h5 {
    font-size: 1.9rem;
    letter-spacing: -0.0035em;
    line-height: 1.47;
    @media (min-width: ${props => props.theme.bp.s}) {
      font-size: 2.1rem;
      letter-spacing: -0.0039em;
      line-height: 1.33;
    }
    @media (min-width: ${props => props.theme.bp.m}) {
      font-size: 2.3rem;
      letter-spacing: -0.0043em;
      line-height: 1.39;
    }
    @media (min-width: ${props => props.theme.bp.l}) {
      font-size: 2.4rem;
      letter-spacing: -0.0044em;
      line-height: 1.33;
    }
  }

  h6 {
    font-size: 1.2rem;
    font-family: ${props => props.theme.f.body};
    font-style: normal;
    font-weight: 200;
    line-height: 1.55;
  }

  ul {
    list-style-type: disc;
  }

  ol {
    list-style-type: numbered;
  }

  ol,
  ul {
    letter-spacing: -0.033em;
    padding-left: 2.3rem;
    line-height: 1.55;
  }

  ol li,
  ul li {
    padding-bottom: 1.8rem;
    &:last-child {
      padding-bottom: 0;
    }
  }

  ::selection {
    background: ${props => props.theme.c.colorB};
    color: ${props => props.theme.c.white};
    text-shadow: none;
  }

  input,
  textarea {
    color: ${props => props.theme.c.colorA};
  }

  textarea {
    min-height: 12.4rem;
    resize: none;
  }

  img {
    @media print {
      max-width: 100% !important;
    }
  }
`;

const Global = createGlobalStyle`
	${Normalize}
	${Fonts}
	${GlobalCSS}
`;

export default Global;
