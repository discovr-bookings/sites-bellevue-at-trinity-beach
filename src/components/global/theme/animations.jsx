import { keyframes } from 'styled-components';

export const pulse = keyframes`
  0% {
    transform: scale(0);
    opacity: 0.5;
  }
  100% {
    transform: scale(1);
    opacity: 0;
  }
`;

export const scrollRotate = keyframes`
  0% {
      transform: rotate(0deg)
  }
  50% {
      transform: rotate(180deg)
  }
  100% {
      transform: rotate(360deg)
  }
`;
