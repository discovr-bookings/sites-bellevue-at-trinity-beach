// Breakpoints
export const SmallBp = '768px';
export const MediumBp = '1024px';
export const LargeBp = '1240px';
export const xLargeBp = '1776px';

// Container
export const ContainerWidth = '1920px';
export const ContainerPadding = '24px';

// Font
export const fontBody = `'gt-america', sans-serif`;
export const fontDisplay = `'gt-super', sans-serif`;
