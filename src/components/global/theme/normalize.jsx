import { css } from 'styled-components';

const Normalize = css`
  html {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    -webkit-font-smoothing: antialiased;
  }

  body {
    line-height: 1;
  }

  blockquote {
    quotes: none;
  }

  abbr[title] {
    border-bottom: 1px dotted;
  }

  pre {
    white-space: pre-wrap;
    word-wrap: break-word;
  }

  small {
    font-size: 80%;
  }

  sub,
  sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline;
  }

  sup {
    top: -0.5em;
  }

  sub {
    bottom: -0.25em;
  }

  ul {
    list-style-type: none;
  }

  address {
    font-style: normal;
  }

  a:active,
  a:hover,
  button,
  input,
  select,
  textarea {
    outline: 0;
  }

  svg {
    width: 100%;
    height: 100%;
  }

  table {
    border-collapse: collapse;
    border-spacing: 0;
  }

  img {
    border: 0;
    -ms-interpolation-mode: bicubic;
    vertical-align: middle;
    max-width: 100%;
    height: auto;
  }

  input:first-line,
  select:first-line {
    display: inline-block;
  }

  button,
  input,
  select,
  textarea {
    font-family: inherit;
    font-size: 100%;
    line-height: 1;
  }

  button,
  input,
  label,
  option,
  select,
  textarea {
    vertical-align: baseline;
    *vertical-align: middle;
    cursor: pointer;
    border-radius: 0;
  }

  button,
  input[type='button'],
  input[type='reset'],
  input[type='submit'] {
    -webkit-appearance: button;
    *overflow: visible;
  }

  input[type='text']:active,
  input[type='text']:focus,
  textarea:active,
  textarea:focus {
    cursor: text;
  }

  :focus {
    outline: none;
  }

  .accessibility-enhance:focus {
    outline: 5px auto -webkit-focus-ring-color;
  }

  input[type='email'],
  input[type='password'],
  input[type='tel'],
  input[type='text'],
  textarea {
    background-clip: padding-box;
  }

  button[disabled],
  input[disabled] {
    cursor: default;
  }

  textarea {
    overflow: auto;
    vertical-align: top;
    resize: vertical;
  }

  input[type='search']::-webkit-search-cancel-button,
  input[type='search']::-webkit-search-decoration {
    -webkit-appearance: none;
  }

  [hidden] {
    display: none;
  }

  article,
  aside,
  details,
  figcaption,
  figure,
  footer,
  header,
  hgroup,
  main,
  nav,
  section,
  summary {
    display: block;
  }
`;

export default Normalize;
