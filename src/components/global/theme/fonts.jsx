import { css } from 'styled-components';

const Fonts = css`
  @font-face {
    font-family: 'gt-america';
    src: url('../../../assets/fonts/gtAmerica-light.eot');
    src: url('../../../assets/fonts/gtAmerica-light.eot?#iefix')
        format('embedded-opentype'),
      url('../../../assets/fonts/gtAmerica-light.woff2') format('woff2'),
      url('../../../assets/fonts/gtAmerica-light.woff') format('woff');
    font-weight: 100;
    font-style: normal;
  }

  @font-face {
    font-family: 'gt-america';
    src: url('../../../assets/fonts/gtAmerica-regular.eot');
    src: url('../../../assets/fonts/gtAmerica-regular.eot?#iefix')
        format('embedded-opentype'),
      url('../../../assets/fonts/gtAmerica-regular.woff2') format('woff2'),
      url('../../../assets/fonts/gtAmerica-regular.woff') format('woff');
    font-weight: 400;
    font-style: normal;
  }

  @font-face {
    font-family: 'gt-america';
    src: url('../../../assets/fonts/gtAmerica-medium.eot');
    src: url('../../../assets/fonts/gtAmerica-medium.eot?#iefix')
        format('embedded-opentype'),
      url('../../../assets/fonts/gtAmerica-medium.woff2') format('woff2'),
      url('../../../assets/fonts/gtAmerica-medium.woff') format('woff');
    font-weight: 600;
    font-style: normal;
  }

  @font-face {
    font-family: 'gt-super';
    src: url('../../../assets/fonts/gtSuperDisplay-light.eot');
    src: url('../../../assets/fonts/gtSuperDisplay-light.eot?#iefix')
        format('embedded-opentype'),
      url('../../../assets/fonts/gtSuperDisplay-light.woff2') format('woff2'),
      url('../../../assets/fonts/gtSuperDisplay-light.woff') format('woff');
    font-weight: 600;
    font-style: normal;
  }
`;

export default Fonts;
