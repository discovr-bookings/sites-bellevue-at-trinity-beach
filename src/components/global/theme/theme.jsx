// Colors
import { colorA, colorB, black, white } from './colors';

// Constants
import {
  fontBody,
  fontDisplay,
  SmallBp,
  MediumBp,
  LargeBp,
  xLargeBp,
} from './constants';

const global = {
  c: {
    colorA,
    colorB,
    black,
    white,
  },
  bp: {
    s: SmallBp,
    m: MediumBp,
    l: LargeBp,
    xl: xLargeBp,
  },
  f: {
    body: fontBody,
    display: fontDisplay,
  },
};

export default global;
