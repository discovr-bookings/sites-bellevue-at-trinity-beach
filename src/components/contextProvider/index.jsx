import React, { createContext, useState, useEffect } from 'react';

export const GlobalConsumer = createContext();

export const GlobalProvider = ({ children, location }) => {
  const [state, setState] = useState({
    locationPath: location.pathname,
    isHome: location.pathname === '/',
    geoLocation: [],
    scrolling: false,
    scrollInit: false,
  });

  useEffect(() => {
    const url = `//ipapi.co/json/`;
    fetch(url)
      .then(data => data.json())
      .then(data => {
        setState(prevState => ({ ...prevState, geoLocation: data }));
      });
  }, []);

  return (
    <GlobalConsumer.Provider
      value={{
        dataContext: state,
      }}
    >
      {children}
    </GlobalConsumer.Provider>
  );
};
