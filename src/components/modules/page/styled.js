import styled, { css } from 'styled-components';

const AllPages = css`
  padding-top: 9.7rem;
  padding-bottom: 9.4rem;
  @media (min-width: ${props => props.theme.bp.s}) {
    padding-top: 12.1rem;
    padding-bottom: 17.6rem;
  }
  @media (min-width: ${props => props.theme.bp.m}) {
    padding-bottom: 22.4rem;
  }
`;

const HomePage = css`
  padding-top: 8rem;
  padding-bottom: 9.4rem;
  @media (min-width: ${props => props.theme.bp.s}) {
    padding-top: 15.8rem;
    padding-bottom: 17.6rem;
  }
  @media (min-width: ${props => props.theme.bp.m}) {
    padding-top: 20rem;
    padding-bottom: 22.4rem;
  }
`;

export const Wrapper = styled.main`
  background-color: ${props => props.theme.c.white};
  margin: 0 auto;
  position: relative;
  z-index: 3;
  ${props => (props.isHome ? HomePage : AllPages)}
`;
