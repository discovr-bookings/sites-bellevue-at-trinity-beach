import React, { useContext } from 'react';

import { Wrapper } from './styled';
import { GlobalConsumer } from '../../contextProvider';

const Page = ({ children }) => {
  const { dataContext } = useContext(GlobalConsumer);
  const { isHome } = dataContext;
  return <Wrapper isHome={isHome}>{children}</Wrapper>;
};
export default Page;
