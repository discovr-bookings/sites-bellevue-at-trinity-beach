import React from 'react';

import { Wrapper } from './styled';

const SiteWrapper = ({ children }) => <Wrapper>{children}</Wrapper>;

export default SiteWrapper;
