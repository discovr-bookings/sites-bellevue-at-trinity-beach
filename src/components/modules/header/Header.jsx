import React, { useContext } from 'react';

import { GlobalConsumer } from '../../contextProvider';
import { ScrollWrapper, SiteHeader, ContainerHeader } from './styled';

const HeaderContents = ({ fixed, scrolling }) => (
  <SiteHeader fixed={fixed}>
    <ContainerHeader fixed={fixed} scrolling={scrolling} />
  </SiteHeader>
);

const Header = ({ fixed }) => {
  const { dataContext } = useContext(GlobalConsumer);
  const { scrolling, scrollInit } = dataContext;
  let Wrapper;
  if (fixed) {
    Wrapper = (
      <ScrollWrapper scrollInit={scrollInit}>
        <HeaderContents fixed scrolling={scrolling} />
      </ScrollWrapper>
    );
  } else {
    Wrapper = <HeaderContents />;
  }
  return Wrapper;
};

export default Header;
