import styled, { css } from 'styled-components';

const ScrollInit = css`
  display: block;
  overflow-y: scroll;
`;

export const ScrollWrapper = styled.div`
	bottom: 0;
	display: none;
	height: 100%;
	left: 0;
	overflow: hidden;
	overflow-y: scroll;
	pointer-events: none;
	position: absolute;
	right: 0;
	top: 0;
	z-index: 50;
	${props => props.scrollInit && ScrollInit}
	@media (min-width: ${props => props.theme.bp.m}) {
		overflow-y: hidden
	}
	&:before {
		content: '';
		height: 10rem;
		left: 0;
		position: absolute;
		right: 0;
		top: 0;
		transform: translateY(-100%);
		transition: transform 0.6s cubic-bezier(0.65, 0.025, 0.67, 0.36);
		z-index: 5;
	}
`;

const FixedHeader = css`
  padding-top: 0.6rem;
  padding-bottom: 0.6rem;
  background: #fff;
  z-index: 3;
  display: block;
  position: absolute;
  top: 0;
  transform: translateY(-100%);
  transition: transform 0.25s cubic-bezier(0.65, 0.025, 0.67, 0.36);
  z-index: 1;
`;

export const SiteHeader = styled.header`
  height: auto;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  transition: opacity 0.4s cubic-bezier(0.65, 0.025, 0.67, 0.36);
  z-index: 50;
  ${props => props.fixed && FixedHeader}
`;

const FixedContainer = css`
  display: block;
  padding-bottom: 1.6rem;
  padding-top: 1.6rem;
  transform: translateY(${props => (props.scrolling ? '0' : '-100%')});
  transition: transform
    ${props =>
      props.scrolling
        ? '0.3s cubic-bezier(0.34, 0.615, 0.4, 0.985)'
        : '0.25s cubic-bezier(0.65, 0.025, 0.67, 0.36)'};
  will-change: transform;

  @media (min-width: ${props => props.theme.bp.l}) {
    padding-bottom: 0;
    padding-top: 0;
  }
`;

export const ContainerHeader = styled.div`
	left: 0;
	padding-bottom: 3.2rem;
	padding-top: 3.2rem;
	position: relative;
	transform: translateY(0);
	width: 100%;
	z-index: 10;
	${props => props.fixed && FixedContainer}
	@media (min-width: ${props => props.theme.bp.s}) {
		padding-bottom: 3.3rem;
		padding-top: 3.3rem;
	}
	@media (min-width: ${props => props.theme.bp.l}) {
		padding-bottom: 5.1rem;
		padding-top: 5.1rem;
	}
`;
