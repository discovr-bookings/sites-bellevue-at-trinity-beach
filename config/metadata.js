module.exports = {
  pathPrefix: '/',
  siteTitle:
    'Trinity Beach Accommodation - Bellevue at Trinity Beach Apartments', // Navigation and Site Title
  siteTitleShort: 'Bellevue at Trinity Beach Apartments', // short_name for manifest
  siteUrl: 'https://www.bellevuetrinitybeach.com.au/', // Domain of site. No trailing slash!
  siteLogo: './src/assets/images/logo/icon.png', // Used for SEO, sharing and manifest
  siteLanguage: 'en', // Language Tag on <html> element
  author: 'Discovr Bookings', // Author for schema.org JSONLD
};
